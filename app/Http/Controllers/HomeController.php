<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

/**
 * Implements class HomeController.
 */
class HomeController extends Controller
{

    /**
     * @return null
     */
    public function index()
    {
        $country = 'Канада';
        $users = User::with('companies')
            ->whereHas('companies', function (Builder $query) use ($country) {
                $query->whereHas('country', function (Builder $query) use ($country) {
                    $query->where('name', $country);
                });
            })
            ->get();

        foreach ($users as $user) {
            print sprintf('User <strong>%s</strong> worked in', $user->name);
            foreach ($user->companies as $idx => $company) {
                print sprintf(
                    '<br />%s. <strong>%s</strong> from <strong>%s</strong>',
                    ($idx + 1),
                    $company->name,
                    $company->pivot->date_started
                );
            }
            print '<hr>';
        }
        dd($users);
    }

}
