<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Implements class Country.
 */
class Country extends Model
{

    /**
     * Get the companies for the country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companies()
    {
        return $this->hasMany(Company::class);
    }

}
