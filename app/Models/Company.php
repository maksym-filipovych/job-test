<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Implements class Company.
 */
class Company extends Model
{

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get the country of the company.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * The users that belong to the company.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_company')
            ->withPivot(['is_active', 'date_started'])
            ->orderBy('date_started', 'DESC');
    }

}
